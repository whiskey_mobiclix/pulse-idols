var app = new Vue({
  el: '#app',
  data: {
    ready: false,
    authenticated: authen(),
  	screen: "activities",
    exporting: false,
    keyword: "",
  },
  mounted(){
    setTimeout(() => {
      this.ready = true
    }, 500)
  },
  computed: {
    title(){
      if(this.screen == "idols") return "Idols list management"
      if(this.screen == "activities") return "Idols data by day"
      if(this.screen == "activities-up-to-now") return "Idols data up to now"
    }
  },
  methods: {
    signout(){
      localStorage.removeItem("pulse-token")
      window.location.reload()
    }
  }
})