const TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE5OTEzNTY0MTQsInVzZXJfaWQiOjc3MTY1NDAsImlzX2FkbWluIjp0cnVlLCJleHRfaW5mbyI6e30sInJvbGVzIjpbXX0.5owdFJ3AJHtWKz6e7MbFaAy3ne0wwcLyukplCiJgZuk"
const TOKEN2 = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE5OTkwMTk5NTMsInVzZXJfaWQiOjExNzUzNTAzLCJpc19hZG1pbiI6ZmFsc2UsImV4dF9pbmZvIjp7fSwicm9sZXMiOltdfQ.a6Hl-xhoDBcUCTzb2NCve3N7TKiFOLZSqaQuaQDieBk"


const getIdolsList = () => {
	return axios.get(`https://cors-anywhere.herokuapp.com/https://pulse-api-v2.mocogateway.com/v1/idol?product=pulse`)
}

const addIdol = uid => {
	return axios.post(`https://cors-anywhere.herokuapp.com/https://pulse-api-v2.mocogateway.com/v1/idol`, {
		userName: [uid],
		product: "pulse",
	})
}

const getActivities = (from, to) => {
	return axios.get(`https://cors-anywhere.herokuapp.com/https://pulse-api-v2.mocogateway.com/v1/engagement/user_idol/analytic?from=${from}&to=${to}&product=pulse`, {
		headers: {
			"Authorization": "Bearer " + TOKEN,
		}
	})
}