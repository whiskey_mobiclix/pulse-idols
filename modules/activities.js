let ActivitiesModule = Vue.component("activities", {
	data: function () {
		return {
			activities: [],
			loading: true,
			date: moment(),
		}
	},
	computed: {
		filteredData(){
			return this.activities.filter(
				o => (
					removeAccents(o.full_name).toLowerCase().indexOf(removeAccents(this.keyword).toLowerCase()) > -1
					||
					o.user_name.indexOf(this.keyword) > -1
				)
			)
		}
	},
	mounted(){
		this.fetch()
	},
	watch: {
		date(){
			this.fetch()
		},
		exporting(){
			this.export()
		}
	},
	methods: {
		fetch(){
			let $this = this
			this.activities = []

			const from = Math.round(moment.utc(this.date.format("DD MMM YYYY 00:00:00")).valueOf() / 1000),
				to = Math.round(moment.utc(this.date.format("DD MMM YYYY 23:59:59")).valueOf() / 1000)

			this.loading = true
			getActivities(from, to).then(res => {
				this.activities = res.data.data.map(o => {
					let d = o
					d.gemsForGifts = $this.gemsForGifts(o.gifts)
					d.gemsForFriendRequest = o.count_accept_friend * 32
					d.gemBySession = Math.floor(o.sum_of_duration / 60 / 20) * 32
					d.totalGemsPerDay = d.gemsForGifts + d.gemsForFriendRequest + d.gemBySession

					return d
				})
			}).finally(() => {
				$this.loading = false
			})
		},
		gemsForGifts(gifts){
			if(!gifts) return 0

			let res = 0
			gifts.forEach(o => {
				res += o.count * o.gift.gems_value
			})

			return res
		},
		exportCSV(){
			if(!this.activities.length) return

			const data = [];
			this.activities.sort((a, b) => {
				if(a.sum_of_duration < b.sum_of_duration) return 1
				return -1
			}).forEach((o, ix) => {
				data.push([
					ix + 1,
					o.user_name,
					o.full_name,
					Math.floor(o.sum_of_duration / 60),
					Math.floor(o.sum_of_duration / 60 / 20) * 32,
					o.count_accept_friend,
					o.count_accept_friend * 32,
					this.gemsForGifts(o.gifts),
					this.gemsForGifts(o.gifts) + Math.floor(o.sum_of_duration / 60 / 20) * 32 + o.count_accept_friend * 32,
				])
			})

		    var csv = ',UID,Name,Session Time,Gems by Session,Accept Friend Request,Gems for Accept Friend Request,Gems for Gifts,Total Gems per day\n';
		    data.forEach(function(row) {
		            csv += row.join(',');
		            csv += "\n";
		    });
		 	
		    var hiddenElement = document.createElement('a');
		    hiddenElement.href = 'data:text/csv;charset=utf-8,' + csv;
		    hiddenElement.target = '_blank';
		    hiddenElement.download = 'idols-data-by-day-'+moment().format("DD-MMM-YYYY")+'.csv';
		    hiddenElement.click();
		}
	},
	props: ["keyword" ],
	template: `
		<div class="card">
			<div class="card-title">
				<div>
					<date-picker></date-picker>
				</div>
				
				<button class="add" @click="exportCSV()">Export CSV</button>
			</div>
			<activities-table :data="filteredData" :date="date" :searching="!!keyword.length" :loading="loading"/>
		</div>
	`
  
})
