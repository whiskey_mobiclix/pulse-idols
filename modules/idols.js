Vue.component("idols", {
	data: function () {
		return {
			creating: false,
			loading: true,
			idols: [],
		}
	},
	computed: {
		filteredIdols(){
			return this.idols.filter(
				o => (
					removeAccents(o.full_name).toLowerCase().indexOf(removeAccents(this.keyword).toLowerCase()) > -1
					||
					o.user_name.indexOf(this.keyword) > -1
				)
			)
		}
	},
	mounted(){
		this.fetch()
	},
	methods: {
		fetch(){
			getIdolsList().then(res => {
				this.idols = res.data.data.sort((a, b) => {
					if(a.full_name < b.full_name) return -1
					return 1
				})
				loading = false
			}).finally(() => {
			})
		},
		toggleCreatePopup(){
			this.creating = !this.creating
		},
		handleSuccess(){
			this.creating = false
			this.fetch()

		}
	},
	props: ["keyword"],
	template: `
		<div class="card">
			<div class="card-title">
				<div></div>

				<button class="add" @click="toggleCreatePopup()">Add new idol</button>
			</div>

			<list :data="filteredIdols" :searching="!!keyword.length"/>
			<create-popup v-if="creating" :oncancel="toggleCreatePopup" :onsuccess="handleSuccess"/>
		</div>
	`
  
})
