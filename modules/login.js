Vue.component("login", {
	data: function () {
		return {
			loading: false,
	        responseError: false,
	        email: "",
	        password: "",
	        error: {
	          email: "",
	          password: "",
	        },
		}
	},
	methods: {
		validEmail: function (email) {
	        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	        return re.test(email);
	      },
	      validate() {
	        let flag = true;
	        if (!this.email.length) {
	          flag = false;
	          this.error.email = "Please enter your email";
	        } else if (!this.validEmail(this.email)) {
	          flag = false;
	          this.error.email = "Email address is not correct";
	        }

	        if (!this.password.length) {
	          flag = false;
	          this.error.password = "Please enter your password";
	        }

	        return flag;
	      },
	      handleEmail(e) {
	        this.error.email = "";
	        this.responseError = false;
	      },
	      handlePassword(e) {
	        this.error.password = "";
	        this.responseError = false;
	      },
	      submit() {
	        if (!this.validate()) return;

	        const acc = accounts.filter(o => atob(o.email) == this.email && atob(o.password) == this.password)[0] || null


	        if(!!acc){
	        	localStorage.setItem("pulse-token", btoa(btoa(this.email) + "pulsetoken" + btoa(this.password)))
	        	this.$parent.authenticated = acc.name
	        } else {
	        	this.responseError = "Wrong email or password, please try again!"
	        }
	      },
	},
	props: [""],
	template: `
		<div class="app-login">
	      <div class="form-container">
	        <div class="logo">
	          <img src="resources/logo.png" />
	        </div>

	        <div class="form-input">
	          <input
	            placeholder="Email or username"
	            type="email"
	            v-model="email"
	            @keyup="handleEmail($event)"
	            @keyup.enter="submit"
	          />

	          <span class="error" id="email-error" v-if="!!error.email.length"
	            >{{error.email}}</span
	          >

	          <input
	            placeholder="Password"
	            type="password"
	            v-model="password"
	            @keyup="handlePassword($event)"
	            @keyup.enter="submit"
	          />

	          <span class="error" id="password-error" v-if="!!error.password.length"
	            >{{error.password}}</span
	          >
	        </div>

	        <div class="response-error" v-if="!!responseError">
	          {{responseError}}
	        </div>

	        <button @click="submit " v-if="!loading">Sign in</button>
	        <button v-if="!!loading">Signing ...</button>
	      </div>
	    </div>
	`
  
})
