Vue.component("activities-up-to-now", {
	data: function () {
		return {
			raw: [],
			activities: [],
			loading: true,
			date: moment(),
		}
	},
	computed: {
		filteredData(){
			return this.activities.filter(
				o => (
					removeAccents(o.full_name).toLowerCase().indexOf(removeAccents(this.keyword).toLowerCase()) > -1
					||
					o.user_name.indexOf(this.keyword) > -1
				)
			)
		}
	},
	mounted(){
		this.fetch()
	},
	watch: {
		date(){
			this.fetch()
		},
		exporting(){
			this.export()
		}
	},
	methods: {
		moment(){
			return moment()
		},
		fetch(){
			let $this = this

			const start = moment("29 May 2020"), dis = Math.abs(start.diff(moment(), "days"))

			this.loading = true
			for(let i = 0; i < dis; i++){
				const from = Math.round(moment.utc(moment(start).add(i, "d").format("DD MMM YYYY 00:00:00")).valueOf() / 1000),
					to = Math.round(moment.utc(moment(start).add(i, "d").format("DD MMM YYYY 23:59:59")).valueOf() / 1000)

				getActivities(from, to).then(res => {
					$this.raw[i] = res.data.data

					if($this.raw.length == dis){
						$this.activities = $this.total($this.raw)
						$this.loading = false
					}
				}).finally(() => {

				})
			}
		},
		total(data){
			let res = [], $this = this
			data[0].forEach(o => {
				let item = {
					avatar_url: o.avatar_url,
					full_name: o.full_name,
					user_name: o.user_name,
					count_accept_friend: [],
					sum_of_duration: [],
					gifts: [],
				}

				data.forEach(d => {
					usr = d.filter(u => u.user_name == item.user_name)[0] || {}
					item.count_accept_friend.push(usr.count_accept_friend)
					item.sum_of_duration.push(usr.sum_of_duration)
					item.gifts.push($this.gemsForGifts(usr.gifts || []))
				})

				res.push(item)
			})

			res.forEach(o => {
				o.friendRequest = o.count_accept_friend.reduce((a, b) => {
					return a + b
				}, 0)

				o.gemsByFriendRequest = o.count_accept_friend.map(item => item * 32).reduce((a, b) => {
					return a + b
				}, 0)

				o.session = o.sum_of_duration.map(item => Math.floor(item / 60)).reduce((a, b) => {
					return a + b
				}, 0)

				o.gemsBySession = o.sum_of_duration.map(item => Math.floor(item / 60 / 20) * 32).reduce((a, b) => {
					return a + b
				}, 0)

				o.gemsForGifts = o.gifts.reduce((a, b) => {
					return a + b
				}, 0)

				o.totalGemsUpToNow = o.gemsByFriendRequest + o.gemsBySession + o.gemsForGifts
			})

			return res
		},
		gemsForGifts(gifts){
			if(!gifts || typeof gifts != "object") return 0

			let res = 0
			gifts.forEach(o => {
				res += o.count * o.gift.gems_value
			})

			return res
		},
		exportCSV(){
			if(!this.activities.length) return

			const data = [];
			this.activities.sort((a, b) => {
				if(a.totalGemsUpToNow < b.totalGemsUpToNow) return 1
				return -1
			}).forEach((o, ix) => {
				data.push([
					ix + 1,
					o.user_name,
					o.full_name,
					o.session,
					o.gemsBySession,
					o.friendRequest,
					o.gemsByFriendRequest,
					o.gemsForGifts,
					o.gemsForGifts + o.gemsBySession + o.gemsByFriendRequest
				])
			})

		    var csv = ',UID,Name,Session Time,Gems by Session,Accept Friend Request,Gems for Accept Friend Request,Gems for Gifts,Total Gems up to now\n';
		    data.forEach(function(row) {
		            csv += row.join(',');
		            csv += "\n";
		    });
		 	
		    var hiddenElement = document.createElement('a');
		    hiddenElement.href = 'data:text/csv;charset=utf-8,' + csv;
		    hiddenElement.target = '_blank';
		    hiddenElement.download = 'idols-data-up-to-now.csv';
		    hiddenElement.click();
		}
	},
	props: ["keyword" ],
	template: `
		<div class="card">
			<div class="card-title">
				<div>
					<div class="date-info">
						From <b>29 May 2020</b> to <b>{{moment().format("DD MMM YYYY")}}</b>
					</div>
				</div>
				
				<button class="add" @click="exportCSV()">Export CSV</button>
			</div>
			<activities-up-to-now-table :data="filteredData" :date="date" :searching="!!keyword.length" :loading="loading"/>
		</div>
	`
  
})
