Vue.component("status", {
	data: function () {
		return {}
	},
	computed: {
		c(){
			return this.status.toLowerCase()
		}
	},
	methods: {
		
	},
	props: ["status"],
	template: `
		<span class="idol-status" :class="c">{{status}}</span>
	`
  
})
