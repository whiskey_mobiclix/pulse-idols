Vue.component("date-picker", {
	data: function () {
		return {
			start: moment(),
        	end: moment()
		}
	},
	mounted(){
		let $this = this

        $("#reportrange").daterangepicker(
          {
            singleDatePicker: true,
            showDropdowns: true,
            minYear: 1901,
            maxDate: moment(),
            minDate: moment("29 May 2020"),
            autoApply: true,
          },
          $this.handle
        );

        this.handle(this.start, this.end)

        $("#reportrange").on("apply.daterangepicker", (e, picker) => {
        	const date = picker.startDate
        	$this.$parent.date = date
        })
	},
	methods: {
		handle(start, end){
			$("#reportrange span").html(start.format("MMMM D, YYYY"));
		}
	},
	props: [""],
	template: `
		<div id="reportrange">
          <i class="fa fa-calendar"></i>
          <span></span>
          <i class="fa fa-caret-down"></i>
        </div>
	`
})
