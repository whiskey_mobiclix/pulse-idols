Vue.component("not-qualified", {
	data: function () {
		return {}
	},
	computed: {
		show(){
			const today = moment(moment().format("DD MMM YYYY 00:00:00"))
			return moment(this.date) < today
		}
	},
	methods: {
		moment(){
			return moment()
		}	
	},
	props: ["date"],
	template: `
		<img class="not-qualified" src="resources/close.svg" title="Not enoungh 20 mins session time" v-if="show"/>
	`
  
})
