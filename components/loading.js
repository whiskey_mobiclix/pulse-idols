Vue.component("loading", {
	data: function () {
		return {}
	},
	methods: {
		
	},
	props: [""],
	template: `
		<div class="loading">
			<img src="resources/loading.svg" />
			<span>Loading data...</span>
		</div>
	`
  
})
