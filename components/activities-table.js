Vue.component("activities-table", {
	data: function () {
		return {
			sortConfig: {
		        id: "sum_of_duration",
		        direction: "desc",
		    },
		}
	},
	computed: {
		sortedData() {
	      if (this.sortConfig.direction === "desc")
	        return this.data.sort((a, b) => {
	          if (a[this.sortConfig.id] < b[this.sortConfig.id]) return 1;
	          return -1;
	        });
	    	
	      else if (this.sortConfig.direction === "asc")
	        return this.data.sort((a, b) => {
	          if (a[this.sortConfig.id] > b[this.sortConfig.id]) return 1;
	          return -1;
	        });
	    },
	},
	methods: {
		sort(id, direction) {
	      this.sortConfig.id = id;
	      this.sortConfig.direction = direction;
	    },
		gemsForGifts(gifts){
			if(!gifts) return 0

			let res = 0
			gifts.forEach(o => {
				res += o.count * o.gift.gems_value
			})

			return res
		},
		gemsBySession(mins){
			return Math.floor(mins / 20) * 32
		},
		gemsByFriendRequest(friends){
			return friends * 32
		},
		gemsPerday(item){
			return this.gemsForGifts(item.gifts) + this.gemsBySession(item.sum_of_duration / 60) + this.gemsByFriendRequest(item.count_accept_friend)
		},
	},
	props: ["data", "date", "searching", "uptonow", "loading"],
	template: `
		<div>
			<table class="table">
				<tr>
					<th>#</th>
					<th>UID</th>
					<th>Idol's name</th>
					<th style="text-align: right; padding-right: 25px"><sort name="Session time" id="sum_of_duration"></sort></th>
					<th><sort name="Gems by Session" id="gemsBySession"></sort></th>
					<th><sort name="Accept Friend Request" id="count_accept_friend"></sort></th>
					<th><sort name="Gems by Friend request" id="gemsByFriendRequest"></sort></th>
					<th><sort name="Gems for Gifts" id="gemsForGifts"></sort></th>
					<th><sort name="Total Gems per day" id="totalGemsPerDay"></sort></th>
				</tr>

				<tr v-for="(item, ix) in sortedData" v-if="!loading">
					<td>{{ix + 1}}</td>
					<td :title="item.user_name">{{item.user_name}}</td>
					<td>
						<span class="name">
							<span class="avatar" :style="{ backgroundImage: 'url(https://pulse-api-v2.mocogateway.com/static/pulse/document/' + item.avatar_url + ')' }"></span>
							{{item.full_name}}
						</span>
					</td>
					<td style="padding-right: 25px">
						<span class="justify" style="min-width: 100px">
							<span style="min-width: 80px;text-align: right">{{Math.floor(item.sum_of_duration / 60)}} mins</span>
							<not-qualified :date="date" v-if="Math.round(item.sum_of_duration / 60 * 100) / 100 < 20"></not-qualified>
						</span>
					</td>
					<td>
						<span class="justify">{{gemsBySession(item.sum_of_duration / 60)}} <gem /></span>
					</td>
					<td>{{item.count_accept_friend}}</td>
					<td>
						<span class="justify">{{gemsByFriendRequest(item.count_accept_friend)}} <gem /></span>
					</td>
					<td>
						<span class="justify">{{gemsForGifts(item.gifts)}} <gem /></span>
					</td>
					<td>
						<span class="justify">{{gemsPerday(item)}} <gem /></span>
					</td>
				</tr>
			</table>

			<loading v-if="loading"></loading>

			<table-empty v-if="!loading && !data.length"></table-empty>
		</div>
	`
  
})
