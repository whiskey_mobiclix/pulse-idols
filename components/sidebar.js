Vue.component("sidebar", {
	data: function () {
		return {
			menu: [
				{
					name: "Idols data by day",
					key: "activities",
				},
				{
					name: "Idols data up to now",
					key: "activities-up-to-now",
				},
				{
					name: "Idols list",
					key: "idols",
				},
			]
		}
	},
	methods: {
		handleClick(key){
			this.$parent.screen = key
		}
	},
	props: ["screen"],
	template: `
		<div class="sidebar">
			<div class="head">
				<img src="resources/logo.png">
			</div>

			<h3>Menu</h3>
			<div class="items">
				<button v-for="item in menu" :class="{active: item.key == screen}" @click="handleClick(item.key)">{{item.name}}</button>
			</div>

			<span class="cp">
				&copy; Copyright: WeVenture, 2020.
			</span>
		</div>
	`
})
