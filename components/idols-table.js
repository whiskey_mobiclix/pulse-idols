Vue.component("list", {
  data: function () {
    return {
      sortConfig: {
        id: "first_idol_call_moment",
        direction: "desc",
      },
    };
  },
  computed: {
    sortedData() {
      if (this.sortConfig.direction === "desc")
        return this.data.sort((a, b) => {
          if (a[this.sortConfig.id] < b[this.sortConfig.id]) return 1;
          return -1;
        });

      else if (this.sortConfig.direction === "asc")
        return this.data.sort((a, b) => {
          if (a[this.sortConfig.id] > b[this.sortConfig.id]) return 1;
          return -1;
        });
    },
  },
  methods: {
    moment() {
      return moment()
    },
    datef(ts){
    	return moment(ts * 1000).format("DD MMM YYYY")
    },
    sort(id, direction) {
      this.sortConfig.id = id;
      this.sortConfig.direction = direction;
    },
  },
  props: ["data", "searching"],
  template: `
		<div>
			<table class="table">
				<tr>
					<th>#</th>
					<th>UID</th>
					<th>Idol's name</th>
					<th><sort name="Activated date" id="start_to_be_idol"></sort></th>
					<th><sort name="Start date" id="first_idol_call_moment"></sort></th>
					<th>Status</th>
				</tr>

				<tr v-for="(item, ix) in sortedData">
					<td>{{ix + 1}}</td>
					<td :title="item.user_name">{{item.user_name}}</td>
					<td>
						<span class="name">
							<span class="avatar" :style="{ backgroundImage: 'url(' + item.avatar_url + ')' }"></span>
							{{item.full_name}}
						</span>
					</td>
					<td>{{datef(item.start_to_be_idol)}}</td>
					<td>{{datef(item.first_idol_call_moment)}}</td>
					<td>
						<status status="Activated"></status>
					</td>
				</tr>
			</table>

			<loading v-if="!data.length && !searching"></loading>
			<table-empty v-if="!data.length && searching""></table-empty>
		</div>
	`,
});
