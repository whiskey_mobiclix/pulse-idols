Vue.component("sort", {
  data: function () {
    return {
      direction: "desc",
    };
  },
  computed: {
    c() {
      return this.$parent.sortConfig.id === this.id ? "activated" : "disabled";
    },
  },
  methods: {
    handleClick() {
      this.direction = this.direction == "asc" ? "desc" : "asc";
      this.$parent.sort(this.id, this.direction);
    },
  },
  props: ["name", "id"],
  template: `
		<span  class="sort" @click="handleClick">
            <i :class="c" class="fa fa-sort"></i>
            <span v-html="name"></span>
		</span>
	`,
});
