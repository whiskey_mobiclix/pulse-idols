Vue.component("activities-up-to-now-table", {
	data: function () {
		return {
			sortConfig: {
		        id: "totalGemsUpToNow",
		        direction: "desc",
		    },
		}
	},
	computed: {
		sortedData() {
	      if (this.sortConfig.direction === "desc")
	        return this.data.sort((a, b) => {
	          if (a[this.sortConfig.id] < b[this.sortConfig.id]) return 1;
	          return -1;
	        });
	    	
	      else if (this.sortConfig.direction === "asc")
	        return this.data.sort((a, b) => {
	          if (a[this.sortConfig.id] > b[this.sortConfig.id]) return 1;
	          return -1;
	        });
	    },
	},
	methods: {
		sort(id, direction) {
	      this.sortConfig.id = id;
	      this.sortConfig.direction = direction;
	    },
	},
	props: ["data", "date", "searching", "loading"],
	template: `
		<div>
			<table class="table">
				<tr>
					<th>#</th>
					<th>UID</th>
					<th>Idol's name</th>
					<th style="text-align: right; padding-right: 25px"><sort name="Session time" id="session"></sort></th>
					<th><sort name="Gems by Session" id="gemsBySession"></sort></th>
					<th><sort name="Accept Friend Request" id="friendRequest"></sort></th>
					<th><sort name="Gems by Friend request" id="gemsByFriendRequest"></sort></th>
					<th><sort name="Gems for Gifts" id="gemsForGifts"></sort></th>
					<th><sort name="Total Gems up to now" id="totalGemsUpToNow"></sort></th>
				</tr>

				<tr v-for="(item, ix) in sortedData" v-if="!loading">
					<td>{{ix + 1}}</td>
					<td :title="item.user_name">{{item.user_name}}</td>
					<td>
						<span class="name">
							<span class="avatar" :style="{ backgroundImage: 'url(https://pulse-api-v2.mocogateway.com/static/pulse/document/' + item.avatar_url + ')' }"></span>
							{{item.full_name}}
						</span>
					</td>
					<td style="padding-right: 25px">
						<span class="justify" style="min-width: 100px">
							<span style="min-width: 80px;text-align: right">{{item.session}} mins</span>
						</span>
					</td>
					<td>
						<span class="justify">{{item.gemsBySession}} <gem /></span>
					</td>
					<td>{{item.friendRequest}}</td>
					<td>
						<span class="justify">{{item.gemsByFriendRequest}} <gem /></span>
					</td>
					<td>
						<span class="justify">{{item.gemsForGifts}} <gem /></span>
					</td>
					<td>
						{{item.totalGemsUpToNow}}
					</td>
				</tr>
			</table>

			<loading v-if="loading"></loading>

			<table-empty v-if="!loading && !data.length"></table-empty>
		</div>
	`
  
})
