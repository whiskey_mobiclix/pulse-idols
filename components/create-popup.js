Vue.component("create-popup", {
	data: function () {
		return {
			loading: false,
			uid: "",
			error: "",
		}
	},
	methods: {
		submit(){
			this.loading = true
			addIdol(this.uid).then(res => {
				if(res.data.user_err && res.data.user_err.length) throw new Error("error")
				else{
					this.onsuccess && this.onsuccess()
				}
			}).catch(e => {
				this.error = "Can not add idol with the UID above."
			}).finally(() => {
				this.loading = false
			})
		}
	},
	props: ["oncancel", "onsuccess"],
	template: `
		<div class="popup">
			<div class="overlay" @click="!loading && oncancel && oncancel()"></div>
			<div class="popup-body">
				<h3>Add a new idol</h3>

				<div class="frm">
					<label>Idol's UID</label>
					<input type="text" placeholder="Enter idol's UID..." v-model="uid" @keyup.enter="submit()"/>
				</div>

				<div class="frm-error" v-if="!!this.error.length">{{error}}</div>

				<div class="actions">
					<button @click="!loading && oncancel && oncancel()">Cancel</button>
					<button @click="submit()">{{loading ? "Loading..." : "Add"}}</button>
				</div>
			</div>
		</div>
	`
  
})
