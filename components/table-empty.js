Vue.component("table-empty", {
	data: function () {
		return {}
	},
	methods: {
		
	},
	props: [""],
	template: `
		<div class="table-empty">There's no result.</div>
	`
  
})
